
 ###### Maven的仓库管理、依赖管理、继承和聚合等特性为项目的构建提供了一整套完善的解决方案

## Q1：maven仓库之间关系？
    
   - 本地仓库
      - setting.xml文件```localRepository```配置的本地路径
   - 私服
     - 公司局域网搭建的内部专用服务（中央仓库镜像）
   - 中央仓库
     - http://repo1.maven.org/maven2/

###### 查找顺序  本地仓库 > 私服/中央仓库

## Q2：依赖版本?
```
<dependency>
    <groupId></groupId>
    <artifactId></artifactId>
    <version>1.0.0-Snapshot(Release)</version>
</dependency>
```
   - Snapshot 开发版本
   - Release 发布版本
 
 ## Q3：scope范围？
 - compile
    - 默认
    - 运行期有效
    - 打入包中
 - provided
    - 编译期有效
    - 运行期不需提供
    - 不打入包中
 - runtime
    - 编译不需要
    - 运行期有效
    - 需要导入包中（接口与实现分离）
 - test
    - 测试用
    - 不打入包
   
 ## Q4：依赖冲突及解决？
 - 依赖多个不同版本，使用最后一个
 - 其他依赖需要高版本
 
 解决方案：
 - 传递依赖
    - ```mavn dependency:tree``` 提前查看依赖是否冲突
    - ```<dependencyManagement>```管理子模块版本一致
    - ```<exclusions> ```排除依赖
 - 最近依赖 
     - ```<dependency> ```指定版本 
      
 ## Q5：目录结构？
 - src/main 打包到jar/war中 
 - src/test 测试内容不打包
 - src/main/resources 拷贝到目标目录
 
## Q6：生命周期？
  - clean 
    
     清理
  - compile  
    
     编译
  - install  
    
    jar放本地仓库
  - package 
    
    打包
  - deploy  
    
    jar上传私服

## Q7：多环境profile？

### 两种实现方式：
- 文件 

创建profile文件夹

分别建立dev.properties,test.properties,pro.properties
- pom
```
<profiles>
    <profile>
        <id>dev</id>
        <properties>
            <profiles.active>dev</profiles.active>
        </properties>
        <activation>
            <!--设置默认激活-->
            <activeByDefault>true</activeByDefault>
        </activation>
    </profile>
    <profile>
        <id>test</id>
        <properties>
            <profiles.active>test</profiles.active>
        </properties>
     </profile>
     <profile>
         <id>pro</id>
         <properties>
             <profiles.active>pro</profiles.active>
         </properties>
      </profile>
</profiles>
```

资源插件配置
```
<plugins>
    <plugin>
        <artifactId>maven-war-plugin</artifactId>
        <version>2.4</version>
        <configuration>
            <webResources>
                <resource>
                    <directory>src/main/resources/META-INF</directory>
                    <filtering>true</filtering>
                    <targetPath>WEB-INF/classes/META-INF</targetPath>
                    <includes>
                        <include>*.properties</include>
                    </includes>
                </resource> 
            </webResources>
        </configuration>
     </plugin>
</plugins>
```
## Q8：多模块 继承与聚合？
- parent
    - root工程，没代码，只有配置
    - 聚合子模块
    - mvn clean/package等
- web
    - 负责前端展示
    - 依赖service ，spring-mvc    
- service
    - 负责业务逻辑
    - 依赖dao
    - 事务相关配置
- dao 
    - 负责持久化
    - 依赖mybatis等
    - mapper.xml sprig配置房到resources下
## Q9：私服？
Nexus