## 非集成方式

### 步骤

1. pom增加依赖
    ````
    <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger2</artifactId>
                <version>2.9.2</version>
            </dependency>
            <dependency>
                <groupId>io.springfox</groupId>
                <artifactId>springfox-swagger-ui</artifactId>
                <version>2.9.2</version>
     </dependency>
   
2. 配置swagger2Config.java
    -  类增加注解
    ```
     @Configure
     @EnableSwagger2
    ```


# 注解

### @Api
用在请求的类上，表示对类的说明

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |tags        | 类的作用，非空覆盖value值      |   是   |
 |value       | 参数没什么意义     |   是   |
 |description        | 对api资源的描述，在1.5版本后不再支持  |  否    |
 |basePath        | 基本路径可以不配置，在1.5版本后不再支持   |   否   |
 |position     | 如果配置多个Api 想改变显示的顺序位置，在1.5版本后不再支持   |  否    |
 |produces     | 设置MIME类型列表（output）。"application/json, application/xml"    |  否    |
 |consumes     | 设置MIME类型列表（input）。"application/json, application/xml"    | 否    |
 |protocols     | 设置特定协议。 例：http， https， ws， wss  |  否    |
 |authorizations     |  获取授权列表（安全声明）   |  否    |
 |hidden     | 配置为true 将在文档中隐藏   |  否    |

```   
示例：   
@Api(tags="登录请求")
@RequestMapping(value="/highPregnant")
@RestController
public class LoginController {}
```
        
### @ApiOperation
用在请求的方法上，说明方法的用途、作用

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |value        | 方法的用途、作用      |   是   |
 |notes        | 方法的备注说明      |   是   |
 |tags        | 操作标签，非空时将覆盖value的值      |   否   |
 |response        | 响应类型（即返回对象）      |   否   |
 |responseContainer        | 声明包装的响应容器（返回对象类型）。有效值为 "List", "Set" or "Map"。      |   否   |
 |responseReference        | 指定对响应类型的引用。将覆盖任何指定的response（）类      |   否   |
 |httpMethod        | 指定HTTP方法，"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS" and "PATCH"      |   否   |
 |position        | 如果配置多个Api 想改变显示的顺序位置，在1.5版本后不再支持      |   否   |
 |nickname        | 第三方工具唯一标识，默认为空     |   否   |
 |produces        | 设置MIME类型列表（output）     |   否   |
 |consumes        | 设置MIME类型列表（output）     |   否   |
 |protocols     | 设置特定协议。 例：http， https， ws， wss  |  否    |
 |authorizations     |  获取授权列表（安全声明）   |  否    |
 |hidden     | 配置为true 将在文档中隐藏   |  否    |  
 |responseHeaders     | 响应头列表   |  否    |  
 |code     | 响应的HTTP状态代码。默认 200   |  否    |  
 |extensions     | 扩展属性列表数组   |  否    |  

```
示例：
@ApiOperation(value = "登录检测", notes="根据用户名、密码判断该用户是否存在")
@PostMapping(value="/login")
public UserModel login(@RequestParam(value = "name", required = false) String account,
@RequestParam(value = "pass", required = false) String password){}
```

### @ApiImplicitParams 
用在请求的方法上，表示一组参数说明
### @ApiImplicitParam
用在@ApiImplicitParams注解中，指定一个请求参数的各个方面

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |name        | 参数名，参数名称可以覆盖方法参数名称，路径参数必须与方法参数一致      |   是   |
 |value        | 参数的汉字说明、解释      |   是   |
 |required        | 参数是否必须传，默认为false [路径参数必填]     |   是   |
 |paramType        | 参数放在哪个地方     |   是   |
 |dataType        | 参数类型，默认String，其它值dataType="Integer"     |   是   |
 |defaultValue        | 参数的默认值     |   是   |
 |allowableValues        | 限制参数的可接受值。<br />1.以逗号分隔的列表   2、范围值  3、设置最小值/最大值     |   否   |
 |access        |允许从API文档中过滤参数     |   否   |
 |allowMultiple        |指定参数是否可以通过具有多个事件接受多个值，默认为false     |   否   |
 |example        |单个示例     |   否   |
 |examples        |参数示例。仅适用于BodyParameters     |   否   |
 
###### paramType
- header  
    请求参数的获取：@RequestHeader
- query  
    请求参数的获取：@RequestParam
- path（用于restful接口） 
    请求参数的获取：@PathVariable
- body（不常用）
- form（不常用）
        
```
示例：
@ApiOperation(value = "登录检测", notes="根据用户名、密码判断该用户是否存在")
@ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", required = false, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pass", value = "密码", required = false, paramType = "query", dataType = "String")
    })
@PostMapping(value="/login")
public UserModel login(@RequestParam(value = "name", required = false) String account,
        @RequestParam(value = "pass", required = false) String password){}
```
### @ApiModel
用于响应类上，表示一个返回响应数据的信息

（这种一般用在post创建的时候，使用@RequestBody这样的场景，请求参数无法使用@ApiImplicitParam注解进行描述的时候）
### @ApiModelProperty
用在属性上，描述响应类的属性

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |value        | 属性的简要说明      |   是   |
 |name        | 允许覆盖属性名称      |   否   |
 |allowableValues        | 限制参数的可接受值。1.以逗号分隔的列表   2、范围值  3、设置最小值/最大值      |   否   |
 |access        | 允许从API文档中过滤属性。      |   否   |
 |notes        | 目前尚未使用。      |   否   |
 |dataType        | 参数的数据类型。可以是类名或者参数名，会覆盖类的属性名称。      |   否   |
 |required        | 参数是否必传，默认为false。      |   否   |
 |position        | 允许在类中对属性进行排序。默认为0。      |   否   |
 |hidden        | 允许在Swagger模型定义中隐藏该属性。      |   否   |
 |example        | 属性的示例。      |   否   |
 |readOnly        | 将属性设定为只读。      |   否   |
 |reference        | 指定对相应类型定义的引用，覆盖指定的任何参数值。      |   否   |
   
     
```
示例：
@ApiModel(value="用户登录信息", description="用于判断用户是否存在")
public class UserModel implements Serializable{
   private static final long serialVersionUID = 1L;
   /**
    * 用户名
    */
   @ApiModelProperty(value="用户名")
   private String account;
   /**
     * 密码
     */
    @ApiModelProperty(value="密码")
   private String password;
}
```        

### @ApiResponses
用在请求的方法上，表示一组响应
### @ApiResponse
用在@ApiResponses中，一般用于表达一个错误的响应信息

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |code        | 数字，例如400      |   是   |
 |message        | 信息，例如"请求参数没填好"      |   是   |
 |response        | 抛出异常的类      |   是   |
        
```
示例：
@ApiOperation(value = "修改用户信息",notes = "打开页面并修改指定用户信息")
@ApiResponses({
    @ApiResponse(code=400,message="请求参数没填好"),
    @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
})
@PostMapping(value="/update/{id}")
public JsonResult update(@PathVariable String id, UserModel model){}
```        
### @ApiParam
用在请求方法中，描述参数信息

|参数名        | 说明    |  是否常用  |
 |--- | ---| ---|
 |name        | 参数名称，参数名称可以覆盖方法参数名称，路径参数必须与方法参数一致      |   是   |
 |value        | 参数的简要说明     |   是   |
 |defaultValue        | 参数默认值     |   是   |
 |required        | 属性是否必填，默认为false [路径参数必须填]     |   是   |
 |allowableValues        |  限制参数的可接受值。1.以逗号分隔的列表   2、范围值  3、设置最小值/最大值     |   否   |
 |access        |  允许从API文档中过滤参数     |   否   |
 |allowMultiple        |  指定参数是否可以通过具有多个事件接受多个值，默认为false     |   否   |
 |hidden        |  隐藏参数列表中的参数     |   否   |
 |example        |  单个示例     |   否   |
 |examples        |  参数示例。仅适用于BodyParameters     |   否   |

```
 示例：
 @ApiOperation(value = "登录检测", notes="根据用户名、密码判断该用户是否存在")
 @PostMapping(value="/login")
 public UserModel login(@ApiParam(name = "name", value = "用户名", required = false) @RequestParam(value = "name", required = false) String account,
        @ApiParam(name = "pass", value = "密码", required = false) @RequestParam(value = "pass", required = false) String password){}


 或以实体类为参数： 
 @ApiOperation(value = "登录检测", notes="根据用户名、密码判断该用户是否存在")
 @PostMapping(value="/login")
 public UserModel login(@ApiParam(name = "model", value = "用户信息Model") UserModel model){}
```
    