|        | Spring cloud    |  dubbo  |
 |--- | ---| ---|
 |功能        | 微服务完整方案      |   服务治理框架   |
  |通信方式        |REST/Http       |RPC协议|
  |服务注册/发现        |Eureka（AP）       |ZK、Nacos|
   |负载均衡        | Ribbon      |   客户端负载   |
    |容错机制        | 6种      |   6种    |
     |熔断机制        | Hystrix      |   无   |
      |配置中心        | Config      |   Nacos   |
       |网关        | zuul、Gateway      |   无   |
        |服务监控        | Hystrix+Turbine      |   Dubbo+Monitor   |
         |链路监控        | Sleuth+Zipkin      |   无   |
          |多语言        | Rest支付多语言      |   Java   |
          
          
          
 --------------------------------分割线--------------------------------------------------------------         
          
|  组件      | 方案1    |  方案2  |方案3  |
|--- | ---| ---|---|
|服务发现        | Eureka      |   Consul   |etcd、Nacos
|共用组件        |  服务间调用组件Feign、|负载均衡组件Ribbon、|熔断器Hystrix|     
|网关        |性能低Zuul、       |性能高Gateway|自研
 |配置中心        | Config、      |   携程阿波罗   |阿里Nacos
  |全链路监控        | zikpin（不推荐）、      |   Pinpoint（不推荐）、    |Skywalking（推荐）


        