
# Reactive Streams
## Reactive Streams JVM中面向流的库标准和规范：
- 处理无限数量的元素
- 按顺序处理
- 组件之间异步传递
- 强制性非阻塞背压（Backpressure）

### 背压（Backpressure）
- 一种常用策略。
- 使得发布者拥有无限的缓冲区存储元素，用于确保发布者发布元素太快，不会去压制订阅者。

### 响应流（Reactive Streams）组成
- 发布者

    发布元素。
- 订阅者

    消费元素。
- 订阅
    
    在发布者中，订阅被创建时，将与订阅者共享。
- 处理器

    发布者与订阅组之间处理数据。
### 响应编程（Reactive programming）
- 定义
 
    ###### 基于异步和事件驱动的非阻塞程序，垂直通过在JVM内启动少量线程扩展，不是水平通过集群扩展。
- 实现框架Reactor

    |API        | 功能    |  返回元素  |
     |--- | ---| ---|
    |Mono | 实现发布者 | 0或1|
    |flux|实现发布者 |N个 |
    
 ### Webflux 模块
 - 支持响应式HTTP和WebSocket客户端
 - 支持REST，HEML和WebSocket交互等程序
 
# WebFlux

|参数名        | MVC    |  WebFlux  |
 |--- | ---| ---|
|是否阻塞 | 阻塞式 | 非阻塞 |
|是否同步|同步处理|异步处理|
|IO场景|普通|IO密集型|
|资源使用率|低|高|

## 生产特性
- 响应式Api
- 编程模型
- 适用性
- 内嵌容器
- starter组件
- 对日志、web、消息、测试及扩展等

#### 响应Api
 
 1. 将Plublisher作为输入
 2. 在框架内部转换城Reactor类型并处理逻辑
 3. 返回Mono或Flux作为输出
 
#### 适用性
![image text](../images/webFlux/webFlux适用性.png)
- MVC能满足，不需改为WebFlux
- 注意容器支持
- 微服务可以混用
- IO密集型服务需WebFlux

#### 编程模型
- 注解控制层
    
    和MVC一样。也支持@RequestBody注解
- 功能性端点
    
    - 基于lambda轻量级编程模型，控制路由和请求。
    - 全程控制请求--响应生命流程
    
#### 内嵌容器
- 默认netty启动，端口8080
- 支持jetty，Undertow Tomcat

#### Starter 组件
- web
    
    spring flux
- 模板引擎
    
    Thymeleaf
- 存储
    
    redis,mongodb,Cassandra。不支持MYSQL
- 内嵌容器
    
    tomcat,jetty,Undertow