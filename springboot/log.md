## 格式化日志
```
2016-04-13 08:23:50.120  INFO 37397 --- [ main] org.hibernate.Version    : HHH000412: Hibernate Core {4.3.11.Final}
```
输出内容：
- 日期时间 
    
   精确到毫秒
- 日志级别
    
    ERROR,WARN,INFO,DEBUG/TRACE
- 分隔符

    ```----``日志正文开始
- 线程名

    方括号括起来 main
- Logger名

    java类名
- 日志内容

## 控制台输出

springboot默认配置ERROR ，WARN，INFO级别日志输出到控制台。

切换debug方式：
- 启动命令增加参数
    
    java -jar app.jar --debug
- 修改appliaction配置文件

    ```debug: true```
    
## 文件输出
修改配置文件
```
logging:
  file: test.log
  path: /var/log/helloDemo
```
###### 日志文件会在10Mb大小的时候被截断，产生新的日志文件，默认级别为：ERROR、WARN、INFO

## 级别控制

